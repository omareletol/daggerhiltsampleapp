package com.example.currancyconverter.data

import com.example.currancyconverter.data.models.CurrencyResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {
    @GET("/v1/latest?access_key=f2a0d9f0f58d1a3fe295e07c97db5085&format=1")
      suspend fun getRates (
        @Query("base") base: String
    ) : Response<CurrencyResponse>
}