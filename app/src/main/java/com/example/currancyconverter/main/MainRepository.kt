package com.example.currancyconverter.main

import com.example.currancyconverter.data.models.CurrencyResponse
import com.example.currancyconverter.util.Resource

interface MainRepository {
    suspend fun getRates(base: String): Resource<CurrencyResponse>
}